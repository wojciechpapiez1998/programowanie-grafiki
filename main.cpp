#include <iostream>
#include <string>
#include <algorithm>
using std::string;
using std::cout;
using std::cin;


string permutationText(string text) {
    int drawnNumber;
    int drawnNumbers[10];
    char tempChar;
    string newText;
    for (int i = 0; i < text.length(); i++) {
        while (true) {
            drawnNumber = rand() % text.length();
            bool exists =
                    std::find(std::begin(drawnNumbers), std::end(drawnNumbers), drawnNumber) != std::end(drawnNumbers);
            if (!exists) {
                tempChar = text[i];
                text[i] = text[drawnNumber];
                text[drawnNumber] = tempChar;
                break;
            }
        }
    }
    return text;
}

string getEncryptedText(string alphabet, string numbers, string randomAlphabet, string randomNumbers, string input){
    for(char & i : input){
        for(int j=0; j<alphabet.length(); j++){
            if(i == alphabet[j]){
                i = randomAlphabet[j];
                goto out;
            }
        }
        for(int j=0; j<numbers.length(); j++){
            if(i == numbers[j]){
                i = randomNumbers[j];
                goto out;
            }
        }
    out:
        continue;
    }
    return input;
}

void printStats(string text, string alphabet, string numbers){
    int alphabetCounts[26] = {0};
    int numberCounts[10] = {0};
    for(char ch : text){
        for(int i=0; i<alphabet.length(); i++){
            if(ch == alphabet[i]){
                alphabetCounts[i]++;
                break;
            }
        }
        for(int i=0; i<numbers.length(); i++){
            if(ch == numbers[i]){
                numberCounts[i]++;
                break;
            }
        }
    }
    for(int i=0; i<26; i++){
        cout << alphabetCounts[i] << " ";
    }
    for(int i=0; i<10; i++){
        cout << numberCounts[i] << " ";
    }
}

void printText(string text){
    for (char ch: text) {
        cout << ch << " ";
    }
}

int main() {
    srand(time(NULL));
    string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    string numbers = "0123456789";
    printText(alphabet);
    printText(numbers);
    cout << "\n";

    string randomAlphabet = permutationText(alphabet);
    string randomNumbers = permutationText(numbers);
    printText(randomAlphabet);
    printText(randomNumbers);
    cout << "\n";

    string input;
    cout << "Wprowadz tekst: ";
    std::getline(std::cin, input);
    std::transform(input.begin(), input.end(),input.begin(), ::toupper);
    cout << "\n\n";
    cout << "Wprowadzony tekst: " << input << "\n";
    string encryptedText = getEncryptedText(alphabet, numbers, randomAlphabet, randomNumbers, input);
    cout << encryptedText;

    cout << "\n\nStatystki podanego wyrazu: " << "\n";
    printText(alphabet);
    printText(numbers);
    cout << "\n";
    printStats(input, alphabet, numbers);

    cout << "\n\nStatystki zaszyfrowanego wyrazu: " << "\n";
    printText(alphabet);
    printText(numbers);
    cout << "\n";
    printStats(encryptedText, alphabet, numbers);
    return 0;
}
