import numpy as np


def stworz_macierz_wag(macierz_wag, wektor_uczacy):
    for i in range(64):
        for j in range(64):
            if i == j:
                continue
            else:
                macierz_wag[i][j] = (2 * wektor_uczacy[i] - 1) * (2 * wektor_uczacy[j] - 1)
 
 
def funkcja_aktywacji(iloczyn_macierzowy, wektor_testowy):
    wynik = []
    for i in range(64):
        if iloczyn_macierzowy[i] > 0:
            wynik.append(1)
        elif iloczyn_macierzowy[i] == 0:
            wynik.append(wektor_testowy[i])
        else:
            wynik.append(0)
 
    return np.array(wynik)



def main():
    wektor_uczacy = np.array([
        0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0 ,0,
        0, 0, 0, 0, 0, 0, 0, 0,
    ])

    wektor_testowy = np.array([
        0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    ])
    
    macierz_wag = np.zeros(64*64)
    macierz_wag = macierz_wag.reshape(64, 64)
    
    stworz_macierz_wag(macierz_wag, wektor_uczacy)
    iloczyn_macierzowy = np.matmul(macierz_wag, wektor_testowy)
    wynik = funkcja_aktywacji(iloczyn_macierzowy, wektor_testowy)
    macierz_wynik = wynik.reshape(8, 8)
    print(macierz_wynik)
    
if __name__ == "__main__":
    main()